Nicholas Patten
================
### _DevOps Principle Consultant_ - _London, UK_

> **Focusing on DevOps and SRE Transformations for Businesses**

-----

Skills
------
| Infrastructure Automation | Cloud         | CI/ CD    | Container Orchestration | Languages   | VCS      | Artifact Repositories | Database  |
| ------------------------- | ------------- | --------  | ----------------------- | ----------- | -------- | --------------------- | --------- |
| HashiCorp Terraform       | AWS           | GitLab    | Kubernetes              | Python      | Git      | Artifactory           | Oracle DB |
| Ansible                   | Digital Ocean | Jenkins   | AWS EKS                 | Groovy      | Mecurial | Nexus                 | MySQl     |
|                           | Linode        | CloudBees | Docker Swarm            | Java        | SVN      |                       |           |
|                           |               | BitBucket |                         | Javascript  |          |                       |           |
|                           |               | TeamCity  |                         | Bash        |          |                       |           |

Professional Experience
-----------------------

### _2018-Present_ - ECS Digital, London - DevOps and Continuous Delivery Consultant - Tech Lead

Currently working as a Tech Lead within one of our Onsite Teams, organising technical deliveries, advising architectural design/ implementation and mentoring consultants. 

**Projects**
- Self-Service IaC Platform for a financial trading platform
- Self-Service for deploying Federated TeamCity instances for application teams
- Organing the delivery of several Microservices that will help to orchestrate the automated deployment of services within a Bank 

### _2017-2018_ - ECS Digital, London - DevOps and Continuous Delivery Consultant 

Provided consultation on DevOps best practices, hands-on cultural and technical transformations, working primarily onsite with an array of different clients, everything from Financial to Media outlets.
Always engaging with different kinds of people and technologies, keeping ahead of the curve to provide custom solutions for a client that adds value to their business.

Completed many successful projects, including migrating old CI Systems, Infrastructure as Code, Pipeline as Code, auto-scaling build agents and immutable Jenkins instances.

Worked as a standalone consultant and as part of a team, in both instances delivering documentation, weekly reports, and maintaining customer expectations.

**Projects:**
- SDLC for a variety of companies
- Puppet driven deployment of Octopus Tentacles
- Jenkins 1 to Jenkins 2 Migration for a Bank
- Migrated TeamCity to Jenkins 2 Declarative Pipeline for a Media Outlet
- TeamCity Kotlin Configuration Scripts for a Bank

### _2016-2017_ - Mirada, Exeter - Release Manager  

Responsible for the managerial and development aspects of providing releases of the company’s CMS product to various environments to support business continuity.

The management side of this role involves communicating with other teams to ensure that their schedules are on time for the next release and working to resolve any blockers that they may encounter. Spending time with staff, working through their build issues, scheduling problems and discussing best implementation methods for designing their service changes.

Hands-on with development, focusing on designing and implementing build processes, often programmatically declared using languages such as groovy and bash, accompanied by Jenkins’ jobs. Maintenance and improvement of these processes and scripts are core to the continuous delivery of the product.

Technical meetings, with various levels of management, around how best to improve our processes and the benefits of the proposed solutions.

Operational tasks as issues arise in deployment environments, which need to be taken care of before install/deploy processes can continue. Acquired good experience with command line and managing Linux based services such as solr, rabbitmq, Oracle database, sshd and more. Was involved in migrating a bricks and mortar system to a cloud-based container system.

### _2014-2016_ - Mirada, Exeter - Software Developer

Initially started out working as a front-end engineer, building an internal administration tool using the Grails framework.
Moved onto being a backend Java Developer for two platforms within the company, using a combination of Spring Boot, Java 7/8, maven, gradle and oracle.
Spent a lot of time performing operational tasks with Linux servers and Oracle databases.

Education
---------

### _2011-2014_ - Aberystwyth University - Computer Science BSc Hons

**Major Project**

Ms. Pac-Man Artificial Intelligence Software Controller:
Researched and developed an autonomous software controller for the video game, Ms Pac-Man game

**Modules**
- C++, C And Java Programming Paradigms
- C And Unix Programming
- Concepts in Programming
- Agile Methodologies
- Software Development Life-cycle
- Developing Internet-based Applications
- Modelling Persistent Data
- Computing Mathematics

### _2009-2010_ - GCE A Level - St David’s Catholic College

### _2004-2009_ - GCSES - St Teilo’s CIW High School
